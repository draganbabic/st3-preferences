st3-preferences
===============

My Sublime Text 3 preferences file.

Requires: 

* [Material Design theme and color scheme](https://github.com/equinusocio/material-theme)
* [Hack font](http://sourcefoundry.org/hack/)

I am also making heavy use of the [MarkdownEditing plugin](https://github.com/SublimeText-Markdown/MarkdownEditing) in GitHub Flavored Markdown mode, which I set up like so (in the Preferences → Package Settings → MarkDown Editing → Markdown GFM Settings – User): 

```
{
  "extensions":
  [
    "md",
    "mdown",
    "txt",
    "markdown"
  ],
  "color_scheme": "Packages/User/SublimeLinter/base16-ocean.light (SL).tmTheme",
  "wrap_width": 100,
  "line_padding_top": 4,
  "line_padding_bottom": 4,
}
```

The only variable here is the `color_scheme` option which I change around every once in a while. I generally switch between a theme-based color scheme if it supports adequate Markdown syntax coloring (example above), and one of the color schemes provided by the plugin: 

```
"color_scheme": "Packages/MarkdownEditing/MarkdownEditor-Dark.tmTheme",
"color_scheme": "Packages/MarkdownEditing/MarkdownEditor-Yellow.tmTheme"
```

While writing I opt to hide the sidebar (Cmd+k, Cmd+b). 
